import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

import './index.css'

const Countdown = (props) => {

    const [days, setDays] = useState(0)
    const [hours, setHours] = useState(0)
    const [minutes, setMinutes] = useState(0)
    const [seconds, setSeconds] = useState(0)

    useEffect(() => {

        // create interval to render timer
        const interval = setInterval(() => {

            const { endDate } = props

            const then = moment(endDate)
            const now = moment()

            const countdown = moment(then - now)

            const days = (countdown > 0) ? countdown.format('D') : 0
            const hours = (countdown > 0) ? countdown.format('HH') : 0
            const minutes = (countdown > 0) ? countdown.format('mm') : 0
            const seconds = (countdown > 0) ? countdown.format('ss') : 0

            setDays(days)
            setHours(hours)
            setMinutes(minutes)
            setSeconds(seconds)

            if (countdown < 0) clearInterval(interval)

        }, 1000)


        return () => {
            clearInterval(interval)
        }

    })

    return (
        <div className="countdown" >
            <span className="countdown-message">
                Perderai <span className="highlight"> questa opportunità</span> in :
                </span>
            <div className="countdown-wrapper">
                <span className="countdown-item">
                    {days}
                    <span className="countdown-item-label">days</span>
                </span>
                <span className="countdown-item">
                    {hours}
                    <span className="countdown-item-label">hours</span>
                </span>
                <span className="high-focus countdown-item">
                    {minutes}
                    <span className="countdown-item-label">minutes</span>
                </span>
                <span className="contrast-text countdown-item">
                    {seconds}
                    <span className="countdown-item-label">seconds</span>
                </span>
            </div>
        </div>
    )

}

Countdown.propTypes = {
    endDate: PropTypes.string
}

export default Countdown