import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardImage, MDBCardBody, MDBCardText, MDBBtn, MDBCardTitle, MDBCardFooter, MDBInput, MDBIcon } from 'mdbreact'
import './Landing.css';
import { https } from "./axios"
import { Elements, StripeProvider, injectStripe } from 'react-stripe-elements';
import Countdown from "./countdown/countdown"
import products from "./products.json"

import { notification } from 'antd';
import { throwStatement } from '@babel/types';
const stripe = {
  public_key: process.env.REACT_APP_PUBLIC_KEY
}

const random_var = Math.random()
class _Main extends React.Component {
  state = {
    products_selected: [],
    form: {
      email: "",
      phone_number: "",
      coupon: "",
      fullname: ""
    }
  }
  openNotificationWithIcon = (type, message, description) => {
    notification[type]({
      message: message,
      description: description,
      style: {
        borderRadius: "12px"
      },
    });
  };
  handleProductSelection = (products) => {
    debugger;
    if (!this.state.form.email || !this.state.form.fullname) {
      this.openNotificationWithIcon('error', 'Campi mancanti', "Compila tutti i campi richiesti per procerdere con l'ordine")
    } else {
      const payload = {
        products: products.map(item => { return { id: item.id, qty: item.selected_qty } }),
        email: this.state.form.email,
        phone_number: this.state.form.phone_number,
        fullName: this.state.form.fullname,
      }
      https.post("/dev/session", payload)
        .then((res) => {
          console.log(res)
          this.props.stripe.redirectToCheckout({ sessionId: res.data.session.id })
        })
        .catch((err) => {
          this.openNotificationWithIcon('error', 'Si è verificato un errore', err)

        })
    }
  }
  handleChange = e => {
    const { value, name } = e.target
    const { form } = this.state
    form[name] = value
    this.setState({ ...this.state, form })
  }
  selectProduct = product => {
    let { products_selected } = this.state
    let found = products_selected.find(item => (item.id === product.id))
    product.selected_qty++
    let new_products = []
    if (!found) {
      // simply add to the list
      new_products = products_selected
      new_products.push(product)
    } else {
      //  add the product
      new_products = products_selected.filter(item => (item.id !== found.id))
      new_products.push(product)
    }
    this.setState({ ...this.state, products_selected: new_products })
    this.openNotificationWithIcon('success', 'Aggiunto al carrello', `${product.name} è stato aggiunto al carrello!`)
  }
  /**
   * Properly show price 
   */
  getPrice = (priceInCents) => {
    return (priceInCents / 100).toFixed(2)
  }
  /**
  * Properly show quantity 
  */
  getQuantity = (qty) => {
    return Math.round(qty - (random_var * (qty - 2) + 2), 2)
  }
  render() {
    const amount = (this.state.products_selected.reduce((acc, curr) => {
      return acc += curr.price.special * curr.selected_qty
    }, 0) / 100).toFixed(2)
    return (
      <MDBContainer className="landing-container">
        <MDBRow id="section1" className="section">
          <MDBCol className="landing-intro" md="6">
            <span className="title">8 Marzo: Festa della donna</span>
            <span className="description">Regala un sorriso. Rendi questo giorno indimenticabile</span>
            <Countdown endDate={"2020-02-28T00:00:00.000Z"} />
          </MDBCol>
          <MDBCol className="landing-intro_image-showcase" md="6">
            <img src={products[0].images[0]} id="intro-img-1" className="img" alt="Product 1" />
            <img src={products[2].images[0]} id="intro-img-3" className="img" alt="Product 3" />
          </MDBCol>
          <MDBCol className="help-element" md="4">
            <MDBIcon fas size="5x" icon="shopping-cart" />
            <span className="help-title">Ordina</span>
            <span className="help-description">Scegli l'emozione da regalare</span>
          </MDBCol>
          <MDBCol className="help-element" md="4">
            <MDBIcon fab size="5x" icon="cc-mastercard" />
            <span className="help-title">Paga Online</span>
            <span className="help-description">Dimenticati il resto</span>
          </MDBCol>
          <MDBCol className="help-element" md="4">
            <MDBIcon fas size="5x" icon="shopping-bag" />
            <span className="help-title">Ritira in negozio</span>
            <span className="help-description">Risparmia l'attesa</span>
          </MDBCol>
          <MDBCol className="my-4" md="12">
            <MDBCard className="map-card">
              <MDBCardBody>
                <MDBRow>
                  <MDBCol className="map-info" md="4">
                    <MDBCardTitle>I regali che cerchi sono qui!</MDBCardTitle>
                    <p>Le emozioni migliori, sono quelle che raggiugnono il cuore.</p>
                    <p>Regala un sorriso indimenticabile. Ordinalo online e ritiralo comodamente in negozio</p>
                  </MDBCol>
                  <MDBCol className="map-wrapper" md="8">
                    <iframe
                      title="Map"
                      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2812.1482157934743!2d7.711207151774184!3d45.18409586000719!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47886ff57ca057f1%3A0xf6416d5f93970d81!2sLa%20Rosa%20Bianca!5e0!3m2!1sit!2sit!4v1582810597557!5m2!1sit!2sit"
                      width="100%"
                      height="100%"
                      frameBorder="0"
                      allowFullScreen="" />
                  </MDBCol>
                </MDBRow>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        </MDBRow>
        <MDBRow id="section2" className="section">
          <h2>Le nostre emozioni, condensate nella bellezza di un fiore</h2>
          {products.map((product, index) => {
            return (
              <MDBCol key={product.id} >
                <MDBCard id={`card-${index}`} className="product-card" style={{ width: "22rem" }}>
                  <MDBCardImage className="img-fluid product-image" src={product.images[0]} waves />
                  <MDBCardBody>
                    <MDBCardTitle>{product.name}</MDBCardTitle>
                    <MDBCardText>
                      {product.description}
                    </MDBCardText>
                    <MDBBtn onClick={() => this.selectProduct(product)} className="choose-button">Acquista</MDBBtn>
                  </MDBCardBody>
                  <MDBCardFooter>
                    <span className="price-special">€ {this.getPrice(product.price.special)}</span>
                    <span className="price-regular">{this.getPrice(product.price.regular)}</span>
                    <span className="quantity">Disponibili: {this.getQuantity(product.qty)}</span>
                  </MDBCardFooter>
                </MDBCard>
              </MDBCol>
            )
          })}

        </MDBRow>
        <MDBRow id="section3" className="section">
          <MDBCol className="mb-4" md="6">
            <MDBCard id="card-purchase">
              <MDBCardBody>
                <MDBCardTitle>Il tuo carrello</MDBCardTitle>

                {!this.state.products_selected.length ?
                  <span>Aggiungi un sorriso</span>
                  :
                  (<>
                    {this.state.products_selected.map((product) => {
                      return (
                        <MDBRow key={product.id} className="product-cart-showcase">
                          <MDBCol className="image" size="4">
                            <img className="img-fluid cart-image" alt="product selected" src={product.images[0]} />
                          </MDBCol>
                          <MDBCol className="info" size="8">
                            <span className="name">{product.name}</span>
                            <span className="price">€ <span className="unit">{this.getPrice(product.price.special)}</span></span>
                            <span className="quantity">Quantità: <span className="unit">{product.selected_qty}</span></span>
                          </MDBCol>
                        </MDBRow>
                      )
                    })}
                    < MDBCardFooter >
                      <span className="float-left">Totale da pagare: </span>
                      <span className="float-right unit">€ {amount}</span>
                    </MDBCardFooter>
                  </>
                  )
                }
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol className="mb-4" md="6">
            <MDBCard id="card-purchase">
              <MDBCardBody className="grey-text">
                <MDBCardTitle>I tuoi dati</MDBCardTitle>
                <MDBInput onChange={this.handleChange} label="* Nome e Cognome" name="fullname" icon="user-alt" group type="email" validate error="wrong"
                  success="right" />
                <MDBInput onChange={this.handleChange} label="* La tua email" name="email" icon="envelope" group type="email" validate error="wrong"
                  success="right" />
                <MDBInput onChange={this.handleChange} label="  Il tuo numero telefonico" name="phone_number" icon="phone-alt" group type="number" validate />
                <div className="text-center">
                  <MDBBtn onClick={() => this.handleProductSelection(this.state.products_selected)} disabled={!this.state.products_selected.length} className="purchase-button">Paga ora</MDBBtn>
                </div>
                <div className="payment-icon">
                  <MDBIcon fab size="2x" icon="cc-mastercard" />
                  <MDBIcon fab size="2x" icon="cc-stripe" />
                </div>
                <span className="mt-4 d-block">*Pagando accetti i termini e le condizioni del servizio</span>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        </MDBRow>

      </MDBContainer >
    );
  }
}
const Main = injectStripe(_Main)
class Landing extends React.Component {
  render() {
    return (
      <StripeProvider apiKey={stripe.public_key}>
        <Elements>
          <Main parent={this.props} />
        </Elements>
      </StripeProvider>
    )
  }
}
export default Landing;
