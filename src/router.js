
import React from 'react'
import { BrowserRouter, Switch, Redirect, Route } from "react-router-dom"

import Landing from "./Landing"

// define history adn access it everywhere
import { createBrowserHistory } from 'history';
export const history = createBrowserHistory();

export const routing = (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Landing} />
            <Redirect to="/" />
        </Switch>
    </BrowserRouter>
)
